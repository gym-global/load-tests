import http from 'k6/http';
import { check, sleep } from 'k6';

export let options = {
  vus: 1000, // virtual concurrent users
  duration: '10s',

  // stages: [
  //   { duration: '30s', target: 20 },
  //   { duration: '1m30s', target: 10 },
  //   { duration: '20s', target: 0 },
  // ],
};

// ADD PLAYERS ENDPOINT
// const baseUrl = "http://localhost:8080"
const url =  'http://localhost:8080/health';

var params = {
  headers: {
    'Content-Type': 'application/json',
  },
};

export default function () {
  let res = http.get(url);
  check(res, { 'status was 200': (r) => r.status == 200 });
  sleep(1);
}
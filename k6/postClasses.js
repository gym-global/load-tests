import http from 'k6/http';
import { check, sleep } from 'k6';
// import uuid from "./uuid.js"

// import { Faker } from "k6/x/faker"

// let f = new Faker();

export let options = {
  vus: 1, // virtual concurrent users
  duration: '1s',

  // stages: [
  //   { duration: '30s', target: 20 },
  //   { duration: '1m30s', target: 10 },
  //   { duration: '20s', target: 0 },
  // ],
};

// ADD CLASSES ENDPOINT
const baseUrl = "http://localhost:8080/v1"
const url = '/classes';


var params = {
  headers: {
    'Content-Type': 'application/json',
  },
};

export default function () {
  //Generate data
  // const payloadPlayersAutoFixture = JSON.stringify(
  //     {
  //       name: f.name,
  //       start_date: f.date.past(1),
  //       end_date: f.date.past(10),
  //       capacity: f.numeric({ min: 1, max: 100 }),

  //     });

  let payload =
      {
        name: "pilates",
        start_date: "2023-01-01T00:00:00.000000Z",
        end_date: "2023-01-02T00:00:00.000000Z",
        capacity: 30
      };
  
  // const date = new Date();
  // let d = d.toISOString();
  // let uuid4 = uuid.v4();
  // payload.start_date = date
  // payload.end_date = date
  console.log(baseUrl + url)

  let res = http.post(baseUrl + url, JSON.stringify(payload), params);
  check(res, { 'status was 200': (r) => r.status == 200 });
  sleep(1);
}

